#!/bin/sh -e

IMAGE_NAME="eu.gcr.io/$GCLOUD_PROJECT/$BITBUCKET_REPO_SLUG:$BITBUCKET_COMMIT"
TAGGED_IMAGE_NAME="$EXTERNAL_REGISTRY/stif-iev:$BITBUCKET_COMMIT"

echo "$EXTERNAL_REGISTRY_PASSWORD" | docker login "$EXTERNAL_REGISTRY" --username="$EXTERNAL_REGISTRY_USER" --password-stdin

docker pull "$IMAGE_NAME"
docker tag "$IMAGE_NAME" "$TAGGED_IMAGE_NAME"
docker push "$TAGGED_IMAGE_NAME"
