#!/bin/sh -e

namespace="$GCLOUD_NAMESPACE"
if [ -n "$1" ]; then
  namespace="$1"
fi

gcloud container clusters get-credentials "$GCLOUD_CLUSTER" --zone "$GCLOUD_ZONE"

IMAGE_NAME="eu.gcr.io/$GCLOUD_PROJECT/$BITBUCKET_REPO_SLUG:$BITBUCKET_COMMIT"
kubectl set image deployment --namespace="$namespace" iev iev="$IMAGE_NAME" --record
