package mobi.chouette.exchange.netex_stif.validator;

import java.util.Locale;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.sql.Date;

import org.apache.log4j.BasicConfigurator;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.common.JobData;
import mobi.chouette.exchange.netex_stif.JobDataImpl;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.importer.NetexStifImportParameters;
import mobi.chouette.exchange.netex_stif.model.NetexStifObjectFactory;
import mobi.chouette.exchange.netex_stif.validator.ServiceJourneyValidator.PassingTimeComparator;
import mobi.chouette.exchange.report.ActionReport;
import mobi.chouette.exchange.report.ActionReporter;
import mobi.chouette.exchange.report.ActionReporter.FILE_STATE;
import mobi.chouette.exchange.report.IO_TYPE;
import mobi.chouette.exchange.validation.report.ValidationReport;
import mobi.chouette.model.Line;
import mobi.chouette.model.Period;
import mobi.chouette.exchange.netex_stif.model.DayTypeAssignment;
import mobi.chouette.model.CalendarDay;
import mobi.chouette.model.Timetable;
import mobi.chouette.exchange.netex_stif.validator.DayTypeAssignmentValidator;
import mobi.chouette.exchange.netex_stif.model.OperatingPeriod;
import mobi.chouette.model.util.Referential;
import mobi.chouette.persistence.hibernate.ContextHolder;


@Log4j
public class DayTypeAssignmentValidatorTests extends AbstractTest {

	protected static InitialContext initialContext;

	protected Context initImportContext() {
		ContextHolder.setContext("chouette_gui"); // set tenant schema

		Context context = new Context();
		context.put(Constant.INITIAL_CONTEXT, initialContext);
		context.put(Constant.REPORT, new ActionReport());
		context.put(Constant.VALIDATION_REPORT, new ValidationReport());
		NetexStifImportParameters configuration = new NetexStifImportParameters();
		context.put(Constant.CONFIGURATION, configuration);
		context.put(Constant.REFERENTIAL, new Referential());
		context.put(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY, new NetexStifObjectFactory());
		configuration.setName("name");
		configuration.setUserName("userName");
		configuration.setNoSave(true);
		configuration.setOrganisationName("organisation");
		configuration.setReferentialName("test");
		JobDataImpl test = new JobDataImpl();
		context.put(Constant.JOB_DATA, test);
		context.put(Constant.FILE_NAME, "calendriers.xml");
		ActionReporter reporter = ActionReporter.Factory.getInstance();
		reporter.addFileReport(context, "calendriers.xml", IO_TYPE.INPUT);

		test.setAction(JobData.ACTION.importer);
		test.setType("netex_stif");
		context.put(Constant.TESTNG, "true");
		context.put(Constant.OPTIMIZED, Boolean.FALSE);
		return context;
	}

	@BeforeSuite
	public void init() {
		BasicConfigurator.resetConfiguration();
		BasicConfigurator.configure();
		Locale.setDefault(Locale.ENGLISH);
		if (initialContext == null) {
			try {
				initialContext = new InitialContext();
			} catch (NamingException e) {
				e.printStackTrace();
			}

		}
	}

	@Test( description = "DayTypeAssignment IsAvailable control")
	public void verifyIsAvailable() throws Exception {
		Context context = initImportContext();
		DayTypeAssignmentValidator dayTypeAssignmentValidator = (DayTypeAssignmentValidator) ValidatorFactory.getValidator(context, DayTypeAssignmentValidator.class);

		DayTypeAssignment fakeDayTypeAssignment = new DayTypeAssignment();
		fakeDayTypeAssignment.setObjectId("CITYWAY:DayTypeAssignment:14:LOC");
		fakeDayTypeAssignment.setOperatingPeriodRef("CITYWAY:OperatingPeriod:1234:LOC");
		fakeDayTypeAssignment.setIsAvailable(false);
		boolean result = dayTypeAssignmentValidator.check2NeTExSTIFDayTypeAssignment2(context, fakeDayTypeAssignment, 1, 2);
		Assert.assertFalse(result, "validation should be not ok");
		checkReports(context, "calendriers.xml", NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_2, "2_netexstif_daytypeassignment_2", null, FILE_STATE.ERROR);
	}


	@Test( description = "DayTypeAssignment duplicated dates")
	public void verifyDuplicatedDates() throws Exception {
		Context context = initImportContext();

		Date date = Date.valueOf("2015-03-31");
		Timetable timetable = new Timetable();
		timetable.setObjectId("CITYWAY:Timetable:1234:LOC");
		timetable.setComment("nom du calendrier");
		timetable.addCalendarDay(new CalendarDay(date, false));
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		referential.getTimetables().put("CITYWAY:Timetable:1234:LOC", timetable);

		DayTypeAssignmentValidator dayTypeAssignmentValidator = (DayTypeAssignmentValidator) ValidatorFactory.getValidator(context, DayTypeAssignmentValidator.class);

		DayTypeAssignment fakeDayTypeAssignment = new DayTypeAssignment();
		fakeDayTypeAssignment.setObjectId("CITYWAY:DayTypeAssignment:14:LOC");
		fakeDayTypeAssignment.setDayTypeRef("CITYWAY:Timetable:1234:LOC");
		fakeDayTypeAssignment.setDay(new CalendarDay(date, true));
		fakeDayTypeAssignment.setIsAvailable(true);
		boolean result = dayTypeAssignmentValidator.check2NeTExSTIFDayTypeAssignment3(context, fakeDayTypeAssignment, 1, 2);
		Assert.assertFalse(result, "validation should be not ok");
		checkReports(context, "calendriers.xml", NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_3, "2_netexstif_daytypeassignment_3", null, FILE_STATE.ERROR);

		fakeDayTypeAssignment.setObjectId("CITYWAY:DayTypeAssignment:14:LOC");
		fakeDayTypeAssignment.setDayTypeRef("CITYWAY:Timetable:1234:LOC");
		fakeDayTypeAssignment.setDay(new CalendarDay(Date.valueOf("2015-04-31"), true));
		fakeDayTypeAssignment.setIsAvailable(true);
		result = dayTypeAssignmentValidator.check2NeTExSTIFDayTypeAssignment3(context, fakeDayTypeAssignment, 1, 2);
		Assert.assertTrue(result, "validation should be ok");
	}

  @Test( description = "DayTypeAssignment duplicated periods")
	public void verifyDuplicatedPeriods() throws Exception {
		Context context = initImportContext();

    Period period = new Period();
    period.setStartDate(Date.valueOf("2015-03-15"));
    period.setEndDate(Date.valueOf("2015-03-30"));
		Timetable timetable = new Timetable();
		timetable.setObjectId("CITYWAY:Timetable:1234:LOC");
		timetable.setComment("nom du calendrier");
		timetable.addPeriod(period);
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		referential.getTimetables().put("CITYWAY:Timetable:1234:LOC", timetable);

		DayTypeAssignmentValidator dayTypeAssignmentValidator = (DayTypeAssignmentValidator) ValidatorFactory.getValidator(context, DayTypeAssignmentValidator.class);

		DayTypeAssignment fakeDayTypeAssignment = new DayTypeAssignment();
    OperatingPeriod fakeOperatingPeriod = new OperatingPeriod();
		fakeDayTypeAssignment.setObjectId("CITYWAY:DayTypeAssignment:14:LOC");
		fakeDayTypeAssignment.setDayTypeRef("CITYWAY:Timetable:1234:LOC");
    fakeOperatingPeriod.setObjectId("CITYWAY:OperatingPeriod:1:LOC");
    fakeOperatingPeriod.setPeriod(period);
		fakeDayTypeAssignment.setOperatingPeriodRef("CITYWAY:OperatingPeriod:1:LOC");
		fakeDayTypeAssignment.setIsAvailable(true);
		boolean result = dayTypeAssignmentValidator.check2NeTExSTIFDayTypeAssignment4(context, fakeDayTypeAssignment, 1, 2, fakeOperatingPeriod);
		Assert.assertFalse(result, "validation should be not ok");
		checkReports(context, "calendriers.xml", NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_4, "2_netexstif_daytypeassignment_4", null, FILE_STATE.ERROR);

    Period outside_period = new Period();
    outside_period.setStartDate(Date.valueOf("2015-03-10"));
    outside_period.setEndDate(Date.valueOf("2015-03-14"));
    fakeOperatingPeriod.setPeriod(outside_period);
		result = dayTypeAssignmentValidator.check2NeTExSTIFDayTypeAssignment4(context, fakeDayTypeAssignment, 1, 2, fakeOperatingPeriod);
		Assert.assertTrue(result, "validation should be ok");
	}

}
