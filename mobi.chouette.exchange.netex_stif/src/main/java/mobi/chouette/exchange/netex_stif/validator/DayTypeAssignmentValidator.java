package mobi.chouette.exchange.netex_stif.validator;

import java.sql.Date;

import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.DayTypeAssignment;
import mobi.chouette.exchange.validation.report.DataLocation;
import mobi.chouette.exchange.validation.report.ValidationReporter;
import mobi.chouette.model.Timetable;
import mobi.chouette.model.util.Referential;
import mobi.chouette.model.CalendarDay;
import mobi.chouette.model.Period;
import mobi.chouette.exchange.netex_stif.model.OperatingPeriod;
import org.apache.log4j.Logger;

public class DayTypeAssignmentValidator extends AbstractValidator {

	public static final String LOCAL_CONTEXT = NetexStifConstant.DAY_TYPE_ASSIGNMENT;
	private static final Logger logger = Logger.getLogger(DayTypeAssignmentValidator.class);

	protected String getLocalContext() {
		return LOCAL_CONTEXT;
	}

	@Override
	public void init(Context context) {
		super.init(context);
		ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();

		// -- preset checkpoints to OK if uncheck
		validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_1);
		validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_2);
		validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_3);
    validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_4);
	}

	public boolean validate(Context context, DayTypeAssignment dayTypeAssignment, int lineNumber, int columnNumber, OperatingPeriod period) {
		boolean result4 = checkModification(context, NetexStifConstant.DAY_TYPE_ASSIGNMENT, dayTypeAssignment, lineNumber, columnNumber);
		boolean result1 = check2NeTExSTIFDayTypeAssignment1(context, dayTypeAssignment, lineNumber, columnNumber);
		boolean result2 = check2NeTExSTIFDayTypeAssignment2(context, dayTypeAssignment, lineNumber, columnNumber);
		boolean result3 = check2NeTExSTIFDayTypeAssignment3(context, dayTypeAssignment, lineNumber, columnNumber);
    boolean result5 = check2NeTExSTIFDayTypeAssignment4(context, dayTypeAssignment, lineNumber, columnNumber, period);
		return result1 && result2 && result3 && result4 && result5;
	}

	/**
	 * <b>Titre</b> :[Netex] Contrôle de l'objet DayTypeAssignment :
	 * OperatingDayRef
	 * <p>
	 * <b>R&eacute;ference Redmine</b> :
	 * <a target="_blank" href="https://projects.af83.io/issues/2303">Cartes
	 * #2303</a>
	 * <p>
	 * <b>Code</b> : 2-NeTExSTIF-DayTypeAssignment-1
	 * <p>
	 * <b>Variables</b> : néant
	 * <p>
	 * <b>Prérequis</b> : néant
	 * <p>
	 * <b>Prédicat</b> : La référence OperationDayRef ne doit pas être
	 * renseignée
	 * <p>
	 * <b>Message</b> : {fichier}-Ligne {ligne}-Colonne {Colonne} : l'objet
	 * DayTypeAssignment d'identifiant {objectId} ne peut référencer un
	 * OperatingDay
	 * <p>
	 * <b>Criticité</b> : error
	 * <p>
	 *
	 *
	 * @param context
	 * @return
	 */
	public boolean check2NeTExSTIFDayTypeAssignment1(Context context, DayTypeAssignment dayTypeAssignment,
			int lineNumber, int columnNumber) {
		boolean result = dayTypeAssignment.getOperationDayRef() == null;
		if (!result) {
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, dayTypeAssignment);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_1,NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_1, location);

		}
		return result;
	}

	/**
	 * <b>Titre</b> :[Netex] Contrôle de l'objet DayTypeAssignment : IsAvailable
	 * <p>
	 * <b>R&eacute;ference Redmine</b> :
	 * <a target="_blank" href="https://projects.af83.io/issues/2304">Cartes
	 * #2304</a>
	 * <p>
	 * <b>Code</b> : 2-NeTExSTIF-DayTypeAssignment-2
	 * <p>
	 * <b>Variables</b> : néant
	 * <p>
	 * <b>Prérequis</b> : néant
	 * <p>
	 * <b>Prédicat</b> : L'attribut IsAvailable ne peut pas être renseigné à
	 * 'false' si la référence OperatingPeriodRef est renseignée
	 * <p>
	 * <b>Message</b> : {fichier}-Ligne {ligne}-Colonne {Colonne} : l'objet
	 * DayTypeAssignment d'identifiant {objectId} ne peut référencer un
	 * OperatingPeriod sur la condition IsAvailable à faux.
	 * <p>
	 * <b>Criticité</b> : error
	 * <p>
	 *
	 *
	 * @param context
	 * @return
	 */
	public boolean check2NeTExSTIFDayTypeAssignment2(Context context, DayTypeAssignment dayTypeAssignment,
			int lineNumber, int columnNumber) {
		boolean result = true;
		if (dayTypeAssignment.getIsAvailable() != null)
		{
			if (dayTypeAssignment.getIsAvailable() == Boolean.FALSE)
				result = dayTypeAssignment.getOperatingPeriodRef() == null;
		}
		if (!result) {
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, dayTypeAssignment);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_2,NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_2, location);

		}
		return result;
	}

	/**
	 * <b>Titre</b> :[Netex] Contrôle de l'objet DayTypeAssignment : Duplicated dates
	 * <b>Code</b> : 2-NeTExSTIF-DayTypeAssignment-3
	 * <p>
	 * <b>Variables</b> : néant
	 * <p>
	 * <b>Prérequis</b> : néant
	 * <p>
	 * <b>Prédicat</b> : Les dates ne doivent pas exister en double
	 * <p>
	 * <b>Message</b> : {fichier}-Ligne {ligne}-Colonne {Colonne} : l'objet
	 * DayTypeAssignment d'identifiant {objectId} est un doublon d'une date existante.
	 * <p>
	 * <b>Criticité</b> : error
	 * <p>
	 *
	 *
	 * @param context
	 * @return
	 */
	public boolean check2NeTExSTIFDayTypeAssignment3(Context context, DayTypeAssignment dayTypeAssignment,
			int lineNumber, int columnNumber) {
		boolean result = false;

		// Find the date for a DayTypeAssignment
		CalendarDay day = dayTypeAssignment.getDay();

		if(day!= null)
		{
			Date date = day.getDate();
			// Find if date is already defined in the dates of the TimeTable
			String day_type_ref = dayTypeAssignment.getDayTypeRef();
			Referential referential = (Referential) context.get(Constant.REFERENTIAL);
			Timetable time_table = referential.getTimetables().get(day_type_ref);

			if(time_table != null)
			{
				result = time_table.getCalendarDays().stream().anyMatch(calendarDay -> date.equals(calendarDay.getDate()));
			}
		}

		if (result) {
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, dayTypeAssignment);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_3,NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_3, location);
		}
		return !result;
	}

  	/**
	 * <b>Titre</b> :[Netex] Contrôle de l'objet DayTypeAssignment : Duplicated periods
	 * <b>Code</b> : 2-NeTExSTIF-DayTypeAssignment-4
	 * <p>
	 * <b>Variables</b> : néant
	 * <p>
	 * <b>Prérequis</b> : néant
	 * <p>
	 * <b>Prédicat</b> : Les périodes ne doivent pas exister en double
	 * <p>
	 * <b>Message</b> : {fichier}-Ligne {ligne}-Colonne {Colonne} : l'objet
	 * DayTypeAssignment d'identifiant {objectId} est un doublon d'une période existante.
	 * <p>
	 * <b>Criticité</b> : error
	 * <p>
	 *
	 *
	 * @param context
	 * @return
	 */
	public boolean check2NeTExSTIFDayTypeAssignment4(Context context, DayTypeAssignment dayTypeAssignment,
      int lineNumber, int columnNumber, OperatingPeriod operatingPeriod) {
    boolean result = false;

    // Find the operatingPeriodRef for a DayTypeAssignment
    String operatingPeriodRef = dayTypeAssignment.getOperatingPeriodRef();

    if(operatingPeriodRef!= null)
    {
      // Find if date is already defined in the dates of the TimeTable
      String day_type_ref = dayTypeAssignment.getDayTypeRef();
      Referential referential = (Referential) context.get(Constant.REFERENTIAL);
      Timetable time_table = referential.getTimetables().get(day_type_ref);
      if(time_table != null)
      {
        result = time_table.getPeriods().stream().anyMatch(period -> period.intersect(operatingPeriod.getPeriod()));
      }
    }

    if (result) {
      ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
      String fileName = (String) context.get(Constant.FILE_NAME);
      DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, dayTypeAssignment);
      validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_4,NetexCheckPoints.L2_NeTExSTIF_DayTypeAssignment_4, location);
    }
    return !result;
  }
}
