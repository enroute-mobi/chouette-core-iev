package mobi.chouette.exchange.netex_stif.parser;

import java.util.HashMap;
import java.util.Map;

import org.xmlpull.v1.XmlPullParser;
import org.apache.commons.lang.StringUtils;


import lombok.extern.log4j.Log4j;
import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.common.XPPUtil;
import mobi.chouette.exchange.importer.Parser;
import mobi.chouette.exchange.importer.ParserFactory;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.DestinationDisplay;
import mobi.chouette.exchange.netex_stif.model.Direction;
import mobi.chouette.exchange.netex_stif.model.NetexStifObjectFactory;
import mobi.chouette.model.JourneyPattern;
import mobi.chouette.model.Route;
import mobi.chouette.model.StopPoint;
import mobi.chouette.model.StopAreaLite;
import mobi.chouette.model.util.ChouetteModelUtil;
import mobi.chouette.model.util.ObjectFactory;
import mobi.chouette.model.util.Referential;

@Log4j
public class NetexStructureParser implements Parser {

	@Override
	public void parse(Context context) throws Exception {

		XmlPullParser xpp = (XmlPullParser) context.get(Constant.PARSER);
		while (xpp.nextTag() == XmlPullParser.START_TAG) {
			String name = xpp.getName();
			if (members.containsKey(name)) {
				parseMember(name, xpp, context);
			} else if (parsers.containsKey(name)) {
				parseSimpleMember(name, xpp, context);
			} else {
				XPPUtil.skipSubTree(log, xpp);
			}
		}
		orderStopPointsInRoute(context);
		updateDirectionsToRoute(context);
		updateJourneyPatterns(context);
	}

	private void orderStopPointsInRoute(Context context) {
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		for (Route r : referential.getRoutes().values()) {
			r.getStopPoints().sort((o1,o2) -> o1.getPosition() - o2.getPosition());
			StopAreaLite stopArea = getLastStopAreaInRoute(referential, r);

			if (StringUtils.isEmpty(r.getPublishedName())) {
				r.setPublishedName( (stopArea == null) ? "Defaut" : stopArea.getName() );
			}
			if (StringUtils.isEmpty(r.getName())) {
				r.setName( (stopArea == null) ? "Defaut" : ("inbound".equals(r.getWayback())? NetexStifConstant.STIF_DEFAULT_ROUTE_NAME_PREFIX_INBOUND : NetexStifConstant.STIF_DEFAULT_ROUTE_NAME_PREFIX_OUTBOUND)+" "+r.getPublishedName() );
			}

			for (JourneyPattern jp : r.getJourneyPatterns()) {
				ChouetteModelUtil.refreshDepartureArrivals(jp);
			}
		}
	}

	private StopAreaLite getLastStopAreaInRoute(Referential referential, Route route){
		if (route.getStopPoints()!=null && route.getStopPoints().size()>0) {
			StopPoint lastStopPoint = route.getStopPoints().get(route.getStopPoints().size() - 1);
			StopAreaLite stopArea = referential.findStopAreaExtended(lastStopPoint.getStopAreaId());
			return stopArea;
		}
		else {return null;}
	}

	private void parseSimpleMember(String tag, XmlPullParser xpp, Context context) throws Exception {
		String clazz = parsers.get(tag);
		if (clazz != null) {
			Parser parser = ParserFactory.create(clazz);
			parser.parse(context);
		} else {
			XPPUtil.skipSubTree(log, xpp);
		}
	}

	private void parseMember(String tag, XmlPullParser xpp, Context context) throws Exception {
		String elt = members.get(tag);
		if (elt != null) {
			if (xpp.getName().equals(tag)) {
				while (xpp.nextTag() == XmlPullParser.START_TAG) {
					if (xpp.getName().equals(elt)) {
						String clazz = parsers.get(elt);
						if (clazz != null) {
							Parser parser = ParserFactory.create(clazz);
							parser.parse(context);
						} else {
							XPPUtil.skipSubTree(log, xpp);
						}
					}
				}
			}
		} else {
			XPPUtil.skipSubTree(log, xpp);
		}
	}


	/**
	 * This method is used top update JourneyPattern with destination display
	 * information
	 *
	 * @param context
	 */
	private void updateJourneyPatterns(Context context) {
		NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);

		// Set default value for Name / Published Name to fix IDFM Netex standard oversights
		for (JourneyPattern journeyPattern : referential.getJourneyPatterns().values()) {
			if (StringUtils.isEmpty(journeyPattern.getName())||StringUtils.isEmpty(journeyPattern.getPublishedName())) {
				String defaultValue = "";

				// Check if this JourneyPattern is associated to a DestinationDisplay
				String destinationDisplayKey = factory.getJourneyPatternDestinations().get(journeyPattern.getObjectId());
				DestinationDisplay destinationDisplay = factory.getDestinationDisplay(destinationDisplayKey);

				if (destinationDisplay!=null) { journeyPattern.setRegistrationNumber(destinationDisplay.getPublicCode()); }

				if (destinationDisplay!=null && !StringUtils.isEmpty(destinationDisplay.getFrontText())) {
					defaultValue = destinationDisplay.getFrontText();
				} else if (journeyPattern.getArrivalStopPoint()!=null && journeyPattern.getArrivalStopPoint().getStopAreaId()!=null){
					StopAreaLite stopArea = referential.findStopAreaExtended(journeyPattern.getArrivalStopPoint().getStopAreaId());
						defaultValue =stopArea.getName();
				} else {
					log.warn("Unable to set a default value to JourneyPattern #"+journeyPattern.getId()+" name  / published name !");
				}

				if (StringUtils.isEmpty(journeyPattern.getName())) { 	journeyPattern.setName(defaultValue);	}
				if (StringUtils.isEmpty(journeyPattern.getPublishedName())) {	journeyPattern.setPublishedName(defaultValue); }
			}
		}
	}

	/**
	 * This method is used to update the Route with direction information,
	 * because directions were not parsed at the moment route were parsed
	 *
	 * @param context
	 */
	private void updateDirectionsToRoute(Context context) {

		NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		Map<String, String> elts = factory.getRouteDirections();
		for (Map.Entry<String, String> entry : elts.entrySet()) {
			Route route = ObjectFactory.getRoute(referential, entry.getKey());
			Direction direction = factory.getDirection(entry.getValue());
			route.setPublishedName(direction.getName());
		}

	}

	static Map<String, String> members = new HashMap<>();
	static Map<String, String> parsers = new HashMap<>();

	static {

		// init used parsers
		parsers.put(NetexStifConstant.ROUTE, RouteParser.class.getName());
		parsers.put(NetexStifConstant.DIRECTION, DirectionParser.class.getName());
		parsers.put(NetexStifConstant.SERVICE_JOURNEY_PATTERN, ServiceJourneyPatternParser.class.getName());
		parsers.put(NetexStifConstant.DESTINATION_DISPLAY, DestinationDisplayParser.class.getName());
		parsers.put(NetexStifConstant.SCHEDULED_STOP_POINT, ScheduledStopPointParser.class.getName());
		parsers.put(NetexStifConstant.PASSENGER_STOP_ASSIGNMENT, PassengerStopAssignmentParser.class.getName());
		parsers.put(NetexStifConstant.ROUTING_CONSTRAINT_ZONE, RoutingConstraintZoneParser.class.getName());

		ParserFactory.register(NetexStructureParser.class.getName(), new ParserFactory() {
			private NetexStructureParser instance = new NetexStructureParser();

			@Override
			protected Parser create() {
				return instance;
			}
		});
	}

}
