package mobi.chouette.exchange.netex_stif.validator;

import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.VehicleJourneyStopAssignment;
import mobi.chouette.exchange.validation.report.DataLocation;
import mobi.chouette.exchange.validation.report.ValidationReporter;
import mobi.chouette.model.LineLite;
import mobi.chouette.model.StopPoint;
import mobi.chouette.model.VehicleJourney;
import mobi.chouette.model.StopAreaLite;
import mobi.chouette.model.VehicleJourneyAtStop;
import mobi.chouette.exchange.netex_stif.model.NetexStifObjectFactory;
import mobi.chouette.model.util.Referential;
import mobi.chouette.exchange.netex_stif.parser.VehicleJourneyStopAssignmentParser;

import org.apache.commons.lang.StringUtils;
import java.util.List;
import java.util.ArrayList;


public class VehicleJourneyStopAssignmentValidator extends AbstractValidator {

  public static final String LOCAL_CONTEXT = NetexStifConstant.VEHICLE_JOURNEY_STOP_ASSIGNMENT;

  protected String getLocalContext() {
    return LOCAL_CONTEXT;
  }

  @Override
  public void init(Context context) {
    super.init(context);
    ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();

    // -- preset checkpoints to OK if uncheck
    validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_1);
    validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_2);
    validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_3);
    validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_4);
  }

  public boolean validate(Context context, VehicleJourneyStopAssignment vehicleJourneyStopAssignment, int lineNumber, int columnNumber) {
    /** If every attribute is filled **/
    if(check2NeTExSTIFVehicleJourneyStopAssignment1(context, vehicleJourneyStopAssignment, lineNumber, columnNumber)) {
      /** QuayRef exist, else throw a warning **/
      check2NeTExSTIFVehicleJourneyStopAssignment4(context, vehicleJourneyStopAssignment, lineNumber, columnNumber);
      return check2NeTExSTIFVehicleJourneyStopAssignment2(context, vehicleJourneyStopAssignment, lineNumber, columnNumber) && check2NeTExSTIFVehicleJourneyStopAssignment3(context, vehicleJourneyStopAssignment, lineNumber, columnNumber);
    }
    return false;
  }

  /**
  * <b>Titre</b> :[Netex] Contrôle de l'objet VehicleJourneyStopAssignment : complétude
  * <b>Code</b> : 2-NeTExSTIF-VehicleJourneyStopAssignment-1
  * <b>Variables</b> : néant
  * <b>Prérequis</b> : néant
  * <b>Prédicat</b> : Les attributs ScheduledStopPointRef, QuayRef et VehicleJourneyRef de l'objet VehicleJourneyStopAssignment doivent être renseignés.
  * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne} : l'objet VehicleJourneyStopAssignment d'identifiant {objectId} ne référencie pas les attributs suivants : {attribute}
  * <b>Criticité</b> : error
  */
  public boolean check2NeTExSTIFVehicleJourneyStopAssignment1(Context context, VehicleJourneyStopAssignment vehicleJourneyStopAssignment, int lineNumber, int columnNumber) {
    boolean vehicleJourneyRefPresence = vehicleJourneyStopAssignment.getVehicleJourneyRefList() != null && (vehicleJourneyStopAssignment.getVehicleJourneyRefList().size() > 0) && !StringUtils.isEmpty(vehicleJourneyStopAssignment.getVehicleJourneyRefList().get(0));

    if (!StringUtils.isEmpty(vehicleJourneyStopAssignment.getScheduledStopPointRef())
    && !StringUtils.isEmpty(vehicleJourneyStopAssignment.getQuayRef())
    && vehicleJourneyRefPresence) {
      return true;
    }

    String missingAttributes = StringUtils.isEmpty(vehicleJourneyStopAssignment.getScheduledStopPointRef())? (NetexStifConstant.SCHEDULED_STOP_POINT_REF+", "):"";
    missingAttributes += StringUtils.isEmpty(vehicleJourneyStopAssignment.getQuayRef())? (NetexStifConstant.QUAY_REF+", "):"";
    missingAttributes += (!vehicleJourneyRefPresence) ?  (NetexStifConstant.VEHICLE_JOURNEY_REF+", "):"";
    missingAttributes = missingAttributes.substring(0, missingAttributes.length() - 2);

    ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
    String fileName = (String) context.get(Constant.FILE_NAME);
    LineLite line = (LineLite) context.get(Constant.LINE);
    DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, vehicleJourneyStopAssignment);
    validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_1, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_1, location, missingAttributes);
    return false;
  }


  /**
  * <b>Titre</b> :[Netex] Contrôle de l'objet VehicleJourneyStopAssignment : existence des courses
  * <b>Code</b> : 2-NeTExSTIF-VehicleJourneyStopAssignment-2
  * <b>Variables</b> : néant
  * <b>Prérequis</b> : néant
  * <b>Prédicat</b> : Les attribut(s) VehicleJourneyRef doivent correspondre à des courses présentes dans l'offre
  * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne} : le ou les attributs VehicleJourneyRef de l'objet VehicleJourneyStopAssignment d'identifiants {objectId} référencient une course non présente dans l'offre
  * <b>Criticité</b> : error
  */
  public boolean check2NeTExSTIFVehicleJourneyStopAssignment2(Context context, VehicleJourneyStopAssignment vehicleJourneyStopAssignment, int lineNumber, int columnNumber) {
    Referential referential = (Referential) context.get(Constant.REFERENTIAL);
    ArrayList<String> errorIds =  new ArrayList<String>();
    String idsList = "";
    for (String vjRef : vehicleJourneyStopAssignment.getVehicleJourneyRefList()) {
      if (referential.getVehicleJourneys().get(vjRef)==null) {
        errorIds.add(vjRef);
        idsList +=(vjRef+", ");
      }
    }

    if (errorIds.isEmpty()) { return true; }

    idsList = idsList.substring(0, idsList.length() - 2);

    ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
    String fileName = (String) context.get(Constant.FILE_NAME);
    LineLite line = (LineLite) context.get(Constant.LINE);
    DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, vehicleJourneyStopAssignment);
    validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_2, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_2, location, idsList);
    return false;
  }

  /**
  * <b>Titre</b> :[Netex] Contrôle de l'objet VehicleJourneyStopAssignment : existence de l'arrêt au sein de la / des courses
  * <b>Code</b> : 2-NeTExSTIF-VehicleJourneyStopAssignment-3
  * <b>Variables</b> : néant
  * <b>Prérequis</b> : néant
  * <b>Prédicat</b> : L'attribut ScheduledStopPointRef doit correspondre à un horaire de passage présent dans la/les course(s) référencée(s)
  * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne} : l'attribut ScheduledStopPointRef de l'objet VehicleJourneyStopAssignment d'identifiant {objectId} référencie un horaire de passage non présent dans la / les course(s) {vehicleJourneyRef}
  * <b>Criticité</b> : error
  */

  /**************************************************************/
  // That validation is a bit mind-bending, since the only way to check
  // if that ScheduledStopPoint exist AND is contained within the related VehicleJourney
  // is to compare every stop point that vehicleJourney will pass by (through VehicleJourneyAtStops)
  // with the list of stop points related to ScheduledStopPoint
  /**************************************************************/

  public boolean check2NeTExSTIFVehicleJourneyStopAssignment3(Context context, VehicleJourneyStopAssignment vehicleJourneyStopAssignment, int lineNumber, int columnNumber) {
    Referential referential = (Referential) context.get(Constant.REFERENTIAL);
    NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);

    List<StopPoint> relatedStopPoints = factory.getStopPoints(vehicleJourneyStopAssignment.getScheduledStopPointRef());

    ArrayList<String> errorIds =  new ArrayList<String>();
    String idsList = "";

    for (String vjRef : vehicleJourneyStopAssignment.getVehicleJourneyRefList()) {
        VehicleJourney vehicleJourney = referential.getVehicleJourneys().get(vjRef);
        VehicleJourneyAtStop vjas =  VehicleJourneyStopAssignmentParser.findRelatedVJAS(vehicleJourney, relatedStopPoints);
        if (vjas==null) {
          errorIds.add(vjRef);
          idsList +=(vjRef+", ");
        }
    }

    if (errorIds.isEmpty()) { return true; }
    idsList = idsList.substring(0, idsList.length() - 2);

    ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
    String fileName = (String) context.get(Constant.FILE_NAME);
    LineLite line = (LineLite) context.get(Constant.LINE);
    DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, vehicleJourneyStopAssignment);
    validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_3, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_3, location, idsList);
    return false;
  }

  /**
  * <b>Titre</b> :[Netex] Contrôle de l'objet VehicleJourneyStopAssignment : existence du nouvel arrêt
  * <b>Code</b> : 2-NeTExSTIF-VehicleJourneyStopAssignment-2
  * <b>Variables</b> : néant
  * <b>Prérequis</b> : néant
  * <b>Prédicat</b> : L'attribut QuayRef doit correspondre à un arrêt dans le référentiel d'arrêt
  * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne} : l'attribut QuayRef de l'objet VehicleJourneyStopAssignment d'identifiant {objectId} référencie un arrêt non présent dans le référentiel d'arrêt
  * <b>Criticité</b> : warning
  */
  public boolean check2NeTExSTIFVehicleJourneyStopAssignment4(Context context, VehicleJourneyStopAssignment vehicleJourneyStopAssignment, int lineNumber, int columnNumber) {
    Referential referential = (Referential) context.get(Constant.REFERENTIAL);
    StopAreaLite stopAreaLite = referential.getSharedReadOnlyStopAreas().get(vehicleJourneyStopAssignment.getQuayRef());

    if (stopAreaLite!=null) { return true; }

    ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
    String fileName = (String) context.get(Constant.FILE_NAME);
    LineLite line = (LineLite) context.get(Constant.LINE);
    DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, vehicleJourneyStopAssignment);
    validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_4, NetexCheckPoints.L2_NeTExSTIF_VehicleJourneyStopAssignment_4, location, vehicleJourneyStopAssignment.getQuayRef());
    return false;
  }

}
