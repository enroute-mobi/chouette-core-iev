package mobi.chouette.exchange.netex_stif.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import mobi.chouette.model.ChouetteIdentifiedObject;

public class VehicleJourneyStopAssignment extends ChouetteIdentifiedObject {

	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	private String objectId;

	@Getter
	@Setter
	private Long objectVersion;

	@Getter @Setter
	private String scheduledStopPointRef;

	@Getter @Setter
	private String quayRef;

	@Getter @Setter
	private int lineNumber;

	@Getter @Setter
	private int columnNumber;

	@Getter
	private ArrayList<String> vehicleJourneyRefList = new ArrayList<String>();

	public void addVehicleJourneyRefList (String vjRef) {
		this.vehicleJourneyRefList.add(vjRef);
	}

	public void clear() {
		objectId = null;
		scheduledStopPointRef = null;
		quayRef = null;
    vehicleJourneyRefList = new ArrayList<String>();
		creationTime = new Date();
	}

	@Override
	public String toString () {
		String result =  this.objectId+" - "+this.scheduledStopPointRef+" - "+this.quayRef+" - "+this.lineNumber+ " - "+this.columnNumber;
		for (String vjRef : vehicleJourneyRefList) {
			result+= " - "+vjRef;
		}
		return result;
	}
}
