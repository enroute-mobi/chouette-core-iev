package mobi.chouette.exchange.netex_stif.parser;

import org.xmlpull.v1.XmlPullParser;

import lombok.extern.log4j.Log4j;
import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.common.XPPUtil;
import mobi.chouette.exchange.importer.Parser;
import mobi.chouette.exchange.importer.ParserFactory;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.VehicleJourneyStopAssignment;
import mobi.chouette.exchange.netex_stif.model.NetexStifObjectFactory;

import java.util.Map;

@Log4j
public class NetexHoraireParser implements Parser {

	@Override
	public void parse(Context context) throws Exception {
		XmlPullParser xpp = (XmlPullParser) context.get(Constant.PARSER);
		while (xpp.nextTag() == XmlPullParser.START_TAG) {
			if (xpp.getName().equals(NetexStifConstant.SERVICE_JOURNEYS)) {
				while (xpp.nextTag() == XmlPullParser.START_TAG) {
					if (xpp.getName().equals(NetexStifConstant.SERVICE_JOURNEY)) {
						Parser parser = ParserFactory.create(ServiceJourneyParser.class.getName());
						parser.parse(context);
					} else {
						XPPUtil.skipSubTree(log, xpp);
					}
				}
			} else if (xpp.getName().equals(NetexStifConstant.SERVICE_JOURNEY)){
				Parser parser = ParserFactory.create(ServiceJourneyParser.class.getName());
				parser.parse(context);
			} else if (xpp.getName().equals(NetexStifConstant.VEHICLE_JOURNEY_STOP_ASSIGNMENT)){
				VehicleJourneyStopAssignmentParser parser = (VehicleJourneyStopAssignmentParser) ParserFactory.create(VehicleJourneyStopAssignmentParser.class.getName());
				parser.parse(context);
			}
			else {
				XPPUtil.skipSubTree(log, xpp);
			}
		}
		VehicleJourneyStopAssignmentParser parser = (VehicleJourneyStopAssignmentParser) ParserFactory.create(VehicleJourneyStopAssignmentParser.class.getName());
		NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);
		Map<String, VehicleJourneyStopAssignment> vehicleJourneyStopAssignments = factory.getVehicleJourneyStopAssignments();
		for (VehicleJourneyStopAssignment vjsa : vehicleJourneyStopAssignments.values()) {
			parser.triggerParsingCompletion(context, vjsa);
		}
	}

	static{
		ParserFactory.register(NetexHoraireParser.class.getName(), new ParserFactory() {
			private NetexHoraireParser instance = new NetexHoraireParser();

			@Override
			protected Parser create() {
				return instance;
			}
		});
	}
}
