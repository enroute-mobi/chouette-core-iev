package mobi.chouette.exchange.netex_stif.validator;

import org.apache.commons.lang.StringUtils;
import lombok.extern.log4j.Log4j;

import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.PassengerStopAssignment;
import mobi.chouette.exchange.validation.report.DataLocation;
import mobi.chouette.exchange.validation.report.ValidationReporter;
import mobi.chouette.model.LineLite;
import mobi.chouette.model.StopAreaLite;

import mobi.chouette.model.util.Referential;

@Log4j
public class PassengerStopAssignmentValidator extends AbstractValidator {

	public static final String LOCAL_CONTEXT = NetexStifConstant.PASSENGER_STOP_ASSIGNMENT;

	protected String getLocalContext()
	{
		return LOCAL_CONTEXT;
	}

	@Override
	public void init(Context context) {
		super.init(context);
		ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
		// -- preset checkpoints to OK if uncheck
		validationReporter.prepareCheckPointReport(context, NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_1);
	}

	public boolean validate(Context context, PassengerStopAssignment stopAssignment, int lineNumber, int columnNumber)
	{
		/** Since this method only throw a warning, its result does not matter in the final validation **/
		check2NeTExSTIFPassengerStopAssignment2(context, stopAssignment, lineNumber, columnNumber);
		boolean result = checkModification(context, NetexStifConstant.PASSENGER_STOP_ASSIGNMENT, stopAssignment, lineNumber, columnNumber);
		return check2NeTExSTIFPassengerStopAssignment1(context, stopAssignment, lineNumber, columnNumber) && result;
	}
 	/**
 	 * <b>Titre</b> :[Netex] Contrôle de l'objet PassengerStopAssignment : complétude
 	 * <b>R&eacute;ference Redmine</b> : <a target="_blank" href="https://projects.af83.io/issues/2318">Cartes #2318</a>
 	 * <b>Code</b> : 2-NeTExSTIF-PassengerStopAssignment-1
 	 * <b>Variables</b> :  néant
 	 * <b>Prérequis</b> :  néant
 	 * <b>Prédicat</b> :  Les attributs ScheduledStopPointRef et QuayRef OU StopPlaceRef doivent être renseignés
 	 * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne}, l'attribut {attribut requis} de l'objet PassengerStopAssignment {ObjectId} doit être renseigné
 	 * <b>Criticité</b> :  error
 	 * @param context
 	 * @return
 	 */
 	public boolean check2NeTExSTIFPassengerStopAssignment1(Context context, PassengerStopAssignment stopAssignment, int lineNumber, int columnNumber) {
 		boolean result1 = stopAssignment.getScheduledStopPointRef() != null && !stopAssignment.getScheduledStopPointRef().isEmpty();
 		boolean result2 = !StringUtils.isEmpty(stopAssignment.getQuayRef()) || !StringUtils.isEmpty(stopAssignment.getStopPlaceRef());
 		if (!result1) {
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			LineLite line = (LineLite) context.get(Constant.LINE);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, stopAssignment);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_1,NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_1, location, "ScheduledStopPointRef");
 		}
 		if (!result2)
 		{
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			LineLite line = (LineLite) context.get(Constant.LINE);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, stopAssignment);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_1,NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_1, location, "QuayRef");
 		}

 		return result1 && result2;
 	}

	/**
 	 * <b>Titre</b> :[Netex] Contrôle des attributs  QuayRef et StopPlaceRef de l'objet PassengerStopAssignment
 	 * <b>R&eacute;ference Redmine</b> : https://enroute.atlassian.net/browse/CHOUETTE-153
 	 * <b>Code</b> : 2-NeTExSTIF-PassengerStopAssignment-2
 	 * <b>Variables</b> :  néant
 	 * <b>Prérequis</b> :  néant
 	 * <b>Prédicat</b> :  Les attributs de ScheduledStopPointRef - QuayRef doit référencer une StopArea de type "ZDEP" OU StopPlaceRef doit référencer une StopArea de type "ZDLP"
 	 * <b>Message</b> :  {fichier}-Ligne {ligne}-Colonne {Colonne}, l'attribut {attribut} de l'objet PassengerStopAssignment {ObjectId} définit une référence {objectRef} de type d’arrêt incorrect : {areaType}
 	 * <b>Criticité</b> :  warning
 	 */

 	public boolean check2NeTExSTIFPassengerStopAssignment2(Context context, PassengerStopAssignment stopAssignment, int lineNumber, int columnNumber) {
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		boolean areaTypeMismatch = true;

		String quayRef = stopAssignment.getQuayRef();
		String stopPlaceRef = stopAssignment.getStopPlaceRef();
		boolean isQuayRef = !StringUtils.isEmpty(quayRef);

		/** If getSharedReadOnlyStopAreas doesn't contain the stop area ref, then an error should have already been raised in AbstractController#checkExistsRef **/
		if ((!StringUtils.isEmpty(quayRef) && referential.getSharedReadOnlyStopAreas().containsKey(quayRef) && !"zdep".equals(referential.getSharedReadOnlyStopAreas().get(quayRef).getAreaType())) ||
			(!StringUtils.isEmpty(stopPlaceRef) && referential.getSharedReadOnlyStopAreas().containsKey(stopPlaceRef) && !"zdlp".equals(referential.getSharedReadOnlyStopAreas().get(stopPlaceRef).getAreaType()))) {

			StopAreaLite stopArea = referential.getSharedReadOnlyStopAreas().get(isQuayRef ? quayRef : stopPlaceRef);
			ValidationReporter validationReporter = ValidationReporter.Factory.getInstance();
			String fileName = (String) context.get(Constant.FILE_NAME);
			LineLite line = (LineLite) context.get(Constant.LINE);
			DataLocation location = new DataLocation(fileName, lineNumber, columnNumber, line, stopAssignment);
			String errorAttribute = (isQuayRef ? NetexStifConstant.QUAY_REF : NetexStifConstant.STOP_PLACE_REF);
			String errorAreaType = (isQuayRef ? "zdlp" : "zdep");
			location.setAttribute(errorAttribute);
			validationReporter.addCheckPointReportError(context, null, NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_2 ,NetexCheckPoints.L2_NeTExSTIF_PassengerStopAssignment_2, location, errorAreaType, stopArea.getObjectId());
			return false;
 		}
 		return true;
 	}

}
