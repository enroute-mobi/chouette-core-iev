package mobi.chouette.exchange.netex_stif.parser;

import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import lombok.extern.log4j.Log4j;
import mobi.chouette.common.Constant;
import mobi.chouette.common.Context;
import mobi.chouette.common.XPPUtil;
import mobi.chouette.exchange.importer.Parser;
import mobi.chouette.exchange.importer.ParserFactory;
import mobi.chouette.exchange.netex_stif.NetexStifConstant;
import mobi.chouette.exchange.netex_stif.model.VehicleJourneyStopAssignment;
import mobi.chouette.exchange.netex_stif.validator.VehicleJourneyStopAssignmentValidator;
import mobi.chouette.exchange.netex_stif.validator.ValidatorFactory;
import mobi.chouette.exchange.netex_stif.model.NetexStifObjectFactory;
import mobi.chouette.model.StopPoint;
import mobi.chouette.model.StopAreaLite;
import mobi.chouette.model.VehicleJourney;
import mobi.chouette.model.VehicleJourneyAtStop;
import mobi.chouette.model.util.Referential;

@Log4j
public class VehicleJourneyStopAssignmentParser implements Parser {

	@Override
	public void parse(Context context) throws Exception {
		XmlPullParser xpp = (XmlPullParser) context.get(Constant.PARSER);
    NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);

		VehicleJourneyStopAssignmentValidator validator = (VehicleJourneyStopAssignmentValidator) ValidatorFactory.getValidator(context, VehicleJourneyStopAssignmentValidator.class);

		String id = xpp.getAttributeValue(null, NetexStifConstant.ID);
    VehicleJourneyStopAssignment vehicleJourneyStopAssignment = factory.getVehicleJourneyStopAssignment(id);
		vehicleJourneyStopAssignment.setColumnNumber(xpp.getColumnNumber());
		vehicleJourneyStopAssignment.setLineNumber(xpp.getLineNumber());

		while (xpp.nextTag() == XmlPullParser.START_TAG) {
      if (xpp.getName().equals(NetexStifConstant.VEHICLE_JOURNEY_REF)) {
				String ref = xpp.getAttributeValue(null, NetexStifConstant.REF);
				xpp.getAttributeValue(null, NetexStifConstant.VERSION);
				xpp.nextText();
        vehicleJourneyStopAssignment.addVehicleJourneyRefList(ref);
      } else if (xpp.getName().equals(NetexStifConstant.SCHEDULED_STOP_POINT_REF)) {
				String ref = xpp.getAttributeValue(null, NetexStifConstant.REF);
				xpp.getAttributeValue(null, NetexStifConstant.VERSION);
				xpp.nextText();
        vehicleJourneyStopAssignment.setScheduledStopPointRef(ref);
      } else if (xpp.getName().equals(NetexStifConstant.QUAY_REF)) {
				String ref = xpp.getAttributeValue(null, NetexStifConstant.REF);
				xpp.getAttributeValue(null, NetexStifConstant.VERSION);
				xpp.nextText();
        vehicleJourneyStopAssignment.setQuayRef(ref);
      } else {
				XPPUtil.skipSubTree(log, xpp);
			}
		}
		validator.checkNetexRef(context, vehicleJourneyStopAssignment, NetexStifConstant.VEHICLE_JOURNEY_STOP_ASSIGNMENT, id, vehicleJourneyStopAssignment.getLineNumber(), vehicleJourneyStopAssignment.getColumnNumber());
	}

	/** Since there is no easy way to rollback or loop multiple time on a xml section with XmlPullParser, the only way is to read and store parsed data and use those afterward**/
	public void triggerParsingCompletion(Context context, VehicleJourneyStopAssignment vjsa) {
		Referential referential = (Referential) context.get(Constant.REFERENTIAL);
		VehicleJourneyStopAssignmentValidator validator = (VehicleJourneyStopAssignmentValidator) ValidatorFactory.getValidator(context, VehicleJourneyStopAssignmentValidator.class);
		NetexStifObjectFactory factory = (NetexStifObjectFactory) context.get(NetexStifConstant.NETEX_STIF_OBJECT_FACTORY);

		if (validator.validate(context, vjsa, vjsa.getLineNumber(), vjsa.getColumnNumber())) {
			List<StopPoint> relatedStopPoints = factory.getStopPoints(vjsa.getScheduledStopPointRef());

			for (String vjRef : vjsa.getVehicleJourneyRefList()) {
				VehicleJourney vehicleJourney = referential.getVehicleJourneys().get(vjRef);
				/** First find VJAS to modify **/
				VehicleJourneyAtStop vehicleJourneyAtStop = findRelatedVJAS(vehicleJourney, relatedStopPoints);
				/** Then find referenced StopArea (via QuayRef) **/
				StopAreaLite stopAreaLite = referential.getSharedReadOnlyStopAreas().get(vjsa.getQuayRef());
				if (stopAreaLite!=null) { vehicleJourneyAtStop.setStopAreaId(stopAreaLite.getId()); }
			}
		}
	}

	public static VehicleJourneyAtStop findRelatedVJAS(VehicleJourney vehicleJourney, List<StopPoint> relatedStopPoints) {
		for (VehicleJourneyAtStop vjas : vehicleJourney.getVehicleJourneyAtStops()) {
			if (vjas!=null && vjas.getStopPoint()!=null) {
				for (StopPoint sp : relatedStopPoints) {
					if (sp.getObjectId()!=null && sp.getObjectId().equals(vjas.getStopPoint().getObjectId())) {
						return vjas;
					}
				}
			}
		}
		return null;
	}

	static {
		ParserFactory.register(VehicleJourneyStopAssignmentParser.class.getName(), new ParserFactory() {
			private VehicleJourneyStopAssignmentParser instance = new VehicleJourneyStopAssignmentParser();
			@Override
			protected Parser create() {
				return instance;
			}
		});
	}
}
