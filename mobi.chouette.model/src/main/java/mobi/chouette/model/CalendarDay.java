package mobi.chouette.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.time.DateUtils;


/**
* peculiar date for Timetables
*/
@Log4j
@Embeddable
@NoArgsConstructor

public class CalendarDay implements Serializable, Comparable<CalendarDay> , SignedChouetteObject{

	private static final long serialVersionUID = -1964071056103739954L;

	@Getter
	@Setter
	@Column(name = "checksum")
	private String checksum ;

	@Getter
	@Setter
	@Column(name = "checksum_source")
	private String checksumSource;


	@Getter
	@Setter
	@Column(name = "date")
	private Date date;

	@Getter
	@Setter
	@Column(name = "in_out")
	private Boolean included = Boolean.TRUE;

	public CalendarDay(Date date, boolean included) {
		this.date = date;
		this.included = Boolean.valueOf(included);
	}

	public CalendarDay(CalendarDay day) {
		this.date = new Date(day.date.getTime());
		this.included = day.included;
	}

	@Override
	public int compareTo(CalendarDay o) {
		return getDate().compareTo(o.getDate());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((included == null) ? 0 : included.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (!(obj instanceof CalendarDay)) {	return false; }

		CalendarDay other = (CalendarDay) obj;

		if (this.date == null) {
			if (other.date != null) {	return false; }
		} else if (!DateUtils.isSameDay(this.date,other.date)) {
			return false;
		}

		if (this.included == null) {
			if (other.included != null) {
				return false;
			}
		} else if (!this.included.equals(other.included)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return ("CalendarDay : "+(this.included ? "in":"out")+ " - "+this.date);
	}
}
