package mobi.chouette.model.exporter;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import javax.persistence.SequenceGenerator;
import org.hibernate.annotations.Parameter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import mobi.chouette.common.JobData;
import mobi.chouette.model.ActionMessage;
@Entity
@Table(name = "export_messages")
@NoArgsConstructor
@ToString(callSuper = true)
public class ExportMessage extends ActionMessage {
	private static final long serialVersionUID = -2708006192840323555L;

	public JobData.ACTION getAction() {
		return JobData.ACTION.importer;
	}

	@Getter
	@Setter
	@SequenceGenerator(name = "export_messages_id_seq", sequenceName = "export_messages_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "export_messages_id_seq")
	@Id
	@Column(name = "id", nullable = false)
	protected Long id;

	@Getter
	@Setter
	@Column(name = "export_id")
	private Long taskId;

	@Getter
	@Setter
	@Column(name = "resource_id")
	private Long resourceId;

	@Getter
	@Setter
	@Transient
	private Long checkPointId;

	@Getter
	@Setter
	@Column(name = "criticity")
	@Enumerated(EnumType.STRING)
	private CRITICITY criticity;


	public ExportMessage(Long taskId, Long resouceId) {
		this.taskId = taskId;
		setResourceId(resouceId);
		Timestamp now = new Timestamp(Calendar.getInstance().getTimeInMillis());
		this.setCreationTime(now);

	}

}
